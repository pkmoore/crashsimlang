import os
from setuptools import setup


setup(
    name = "cslang",
    version = "0.0.1",
    author = "Preston Moore",
    author_email = "pkm266@nyu.edu",
    description = ("Compile and run cslang automata"),
    url = "https://github.com/pkmoore/crashsimlang",
    packages=['cslang', 'test'],
    long_description='Long Description Goes Here',
)
