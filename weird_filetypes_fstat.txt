Weird Filetypes (fstat())
================================

Preamble

open($filename, ...) = $desc ->
  o($filname, $desc)

fstat($desc, {..., st_mode=$mode, ...} = $res ->
  fs[$mode&S_IFREG == 0, $res = 0]($desc, $mode)




Register Initialization

filename = <user provided filename>




Inspector

.*; o(!filename, ?desc); .* fs[T, T](!desc, ?mode);




Mutator

# Specify expressions to modify stored register values here
mode &= S_IFLNK

# ! is used to write out values from register rather than matching
o(!filename, ?desc); .* fs[T, T](!desc, !mode);
